package users

type Settings struct {
	FullNameMinSize                 int `json:"FullNameMinSize"`
	FullNameMaxSize                 int `json:"FullNameMaxSize"`
	EmailMinSize                    int `json:"EmailMinSize"`
	EmailMaxSize                    int `json:"EmailMaxSize"`
	PasswordMinSize                 int `json:"PasswordMinSize"`
	PasswordMaxSize                 int `json:"PasswordMaxSize"`
	PasswordMinUppercaseLetters     int `json:"PasswordMinUppercaseLetters"`
	PasswordMinLowercaseLetters     int `json:"PasswordMinLowercaseLetters"`
	PasswordMinNumbers              int `json:"PasswordMinNumbers"`
	PasswordMinSymbols              int `json:"PasswordMinSymbols"`
	ConfirmationTokenTTLSeconds     int `json:"ConfirmationTokenTTLSeconds"`
	PasswordRecoveryTokenTTLSeconds int `json:"PasswordRecoveryTokenTTLSeconds"`
	SoftLockAttempts                int `json:"SoftLockAttempts"`
	SoftLockDurationSeconds         int `json:"SoftLockDurationSeconds"`
}
