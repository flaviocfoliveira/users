package users

import (
	"time"
)

type User struct {
	PublicID           string     `bson:"PublicID"`
	Email              string     `bson:"Email"`
	FullName           string     `bson:"FullName"`
	Password           string     `bson:"Password" json:"-"`
	Salt               string     `bson:"Salt" json:"-"`
	PasswordDate       time.Time  `bson:"PasswordDate"`
	IsActive           bool       `bson:"IsActive"`
	CreatedDate        time.Time  `bson:"CreatedDate"`
	IsConfirmed        bool       `bson:"IsConfirmed"`
	ConfirmedDate      *time.Time `bson:"ConfirmedDate"`
	IsLocked           bool       `bson:"IsLocked"`
	LockedDate         *time.Time `bson:"LockedDate"`
	MustResetPassword  bool       `bson:"MustResetPassword"`
	FailedAuthAttempts int        `bson:"FailedAuthAttempts"`
}

type UserToken struct {
	PublicID   string    `bson:"PublicID"`
	Token      string    `bson:"Token"`
	CreateDate time.Time `bson:"CreateDate"`
	ExpireDate time.Time `bson:"ExpireDate"`
}

type AuthFail struct {
	PublicID   string    `bson:"PublicID"`
	CreateDate time.Time `bson:"CreateDate"`
	ExpireDate time.Time `bson:"ExpireDate"`
}

type SearchRequest struct {
	PublicID          *string `json:"PublicID"`
	Email             *string `json:"Email"`
	FullName          *string `json:"FullName"`
	IsActive          *bool   `json:"IsActive"`
	IsConfirmed       *bool   `json:"IsConfirmed"`
	IsLocked          *bool   `json:"IsLocked"`
	MustResetPassword *bool   `json:"MustResetPassword"`
	OrderBy           *string `json:"OrderBy"`
	OrderByDirection  *string `json:"OrderByDirection"`
	Page              int     `json:"Page"`
	PageSize          int     `json:"PageSize"`
}

type SearchResult struct {
	SearchRequest

	Items []User `json:"Items"`
	Total int64  `json:"Total"`
}
