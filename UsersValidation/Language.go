package UsersValidation

const (
	Required                 = "Required"
	Min                      = "Min"
	Max                      = "Max"
	OnlyLetters              = "OnlyLetters"
	Equal                    = "Equal"
	InvalidValue             = "InvalidValue"
	InvalidEmail             = "InvalidEmail"
	Duplicated               = "Duplicated"
	MinUppercase             = "MinUppercase"
	MinLowercase             = "MinLowercase"
	MinNumeric               = "MinNumeric"
	MinSymbols               = "MinSymbols"
	NotFound                 = "NotFound"
	WrongPassword            = "WrongPassword"
	ConfirmedPassword        = "ConfirmedPassword"
	InvalidConfirmationToken = "InvalidConfirmationToken"
)

type Language map[string]string

func (lang Language) Init() {
	lang[Required] = "Campo obrigatório"
	lang[Min] = "Deve ter no mínimo %v caracteres"
	lang[Max] = "Deve ter no máximo %v caracteres"
	lang[OnlyLetters] = "Deve conter apenas letras"
	lang[Equal] = "Deve ser igual a %s"
	lang[InvalidValue] = "Valor inválido"
	lang[InvalidEmail] = "Email inválido"
	lang[Duplicated] = "Já está a ser usado"
	lang[MinUppercase] = "Deve conter no minimo %v maiúsculas"
	lang[MinLowercase] = "Deve conter no minimo %v minúsculas"
	lang[MinNumeric] = "Deve conter no minimo %v números"
	lang[MinSymbols] = "Deve conter no minimo %v simbolos"
	lang[NotFound] = "%s não encontrado"
	lang[WrongPassword] = "Password errada"
	lang[ConfirmedPassword] = "Password não coincide"
	lang[InvalidConfirmationToken] = "Código de confirmação inválido"
}

func (lang Language) Get(key string, defaultString string) string {

	if r, ok := lang[key]; ok {
		return r
	}

	return defaultString
}
