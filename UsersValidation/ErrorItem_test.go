package UsersValidation

import (
	"testing"
)

func TestErrorItem_Error(t *testing.T) {

	ei := ErrorItem{
		Field:      "f1",
		Rule:       "ruleName",
		RuleValues: []interface{}{120},
	}

	err := ei.Error()
	if err != "f1: ruleName" {
		t.Fail()
	}

}

func TestErrorItem_Translate(t *testing.T) {

	lang := make(Language)
	lang["RuleNameSimple"] = "rule number one is OK"
	lang["RuleNameFormated"] = "formated rule is %s"

	r1 := ErrorItem{
		Field: "f1",
		Rule:  "RuleNameSimple",
	}
	msg1 := r1.Translate(lang)
	if msg1 != "rule number one is OK" {
		t.Fail()
	}

	rf := ErrorItem{
		Field:      "f1",
		Rule:       "RuleNameFormated",
		RuleValues: []interface{}{"also OK"},
	}
	msg2 := rf.Translate(lang)
	if msg2 != "formated rule is also OK" {
		t.Fail()
	}

	rNotFound := ErrorItem{
		Field:      "f1",
		Rule:       "RuleNotFound",
		RuleValues: []interface{}{"also OK"},
	}
	msg3 := rNotFound.Translate(lang)
	if msg3 != "f1: RuleNotFound" {
		t.Fail()
	}

}

func TestErrorItems_Contains(t *testing.T) {

	items := make(ErrorItems, 0)
	items = append(items, ErrorItem{
		Field:      "f1",
		Rule:       "R1",
		RuleValues: nil,
	})

	if !items.Contains("f1", "R1") {
		t.Fail()
	}

	if items.Contains("f1", "RR11") {
		t.Fail()
	}

	if items.Contains("ff11", "R1") {
		t.Fail()
	}

	if items.Contains("ff11", "RR11") {
		t.Fail()
	}

}

func TestErrorItems_Error(t *testing.T) {

	items := make(ErrorItems, 0)

	msg1 := items.Error()
	if msg1 != "" {
		t.Fail()
	}

	items = append(items, ErrorItem{
		Field:      "f1",
		Rule:       "R1",
		RuleValues: nil,
	})
	msg2 := items.Error()
	if msg2 != "f1: R1" {
		t.Fail()
	}

	items = append(items, ErrorItem{
		Field:      "FF11",
		Rule:       "RR11",
		RuleValues: nil,
	})
	msg3 := items.Error()
	if msg3 != "f1: R1 ; FF11: RR11" {
		t.Fail()
	}

}
