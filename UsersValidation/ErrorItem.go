package UsersValidation

import (
	"bytes"
	"fmt"
)

type (
	ErrorItem struct {
		Field      string
		Rule       string
		RuleValues []interface{}
	}

	ErrorItems []ErrorItem
)

func (e ErrorItem) Error() string {
	return fmt.Sprintf("%s: %s", e.Field, e.Rule)
}

func (e ErrorItem) Translate(lang Language) string {

	if ruleMsg, ok := lang[e.Rule]; ok {
		if len(e.RuleValues) == 0 {
			return ruleMsg
		}
		return fmt.Sprintf(ruleMsg, e.RuleValues...)
	}

	return e.Error()
}

func (e ErrorItems) Error() string {
	if l := len(e); l > 0 {
		var b bytes.Buffer

		for ix, err := range e {
			b.WriteString(err.Error())
			if ix < l-1 {
				b.WriteString(" ; ")
			}
		}

		return b.String()
	}
	return ""
}

func (e ErrorItems) Contains(field string, rule string) bool {
	if len(e) > 0 {
		for _, o := range e {
			if o.Field == field && o.Rule == rule {
				return true
			}
		}
	}
	return false
}
