package users

import (
	"context"
	"crypto/sha512"
	"encoding/base64"
	"errors"
	"fmt"
	"regexp"
	"strings"
	"time"

	"bitbucket.org/flaviocfoliveira/audit"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"golang.org/x/crypto/pbkdf2"

	v "bitbucket.org/flaviocfoliveira/users/UsersValidation"
)

const AuditModuleName string = "users.Service"

var UserNotFound = errors.New("UserNotFound")
var UserInactive = errors.New("UserInactive")
var UserLocked = errors.New("UserLocked")
var UserSoftLocked = errors.New("UserSoftLocked")
var UserWrongPassword = errors.New("UserWrongPassword")
var UserMustResetPassword = errors.New("UserMustResetPassword")

var ConfirmationTokenDuplicated = errors.New("ConfirmationTokenDuplicated")
var ConfirmationTokenNotFound = errors.New("ConfirmationTokenNotFound")
var ResetPasswordTokenNotFound = errors.New("ResetPasswordTokenNotFound")

type Service struct {
	Config           *Settings
	client           *mongo.Client
	db               *mongo.Database
	Users            *mongo.Collection
	UsersAuthFailed  *mongo.Collection
	Confirmations    *mongo.Collection
	PasswordRecovery *mongo.Collection
}
type Manager struct {
	AuditContext *audit.AuditContext
	Service      *Service
}

func NewUsersService(conn string, config *Settings) (*Service, error) {
	r := Service{
		Config: config,
	}

	opt := options.Client().ApplyURI(conn)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, opt)
	if err != nil {
		return nil, err
	}

	r.client = client

	db := client.Database(opt.Auth.AuthSource)
	r.db = db

	u := db.Collection("Users")
	r.Users = u

	UQUsersPublicID := mongo.IndexModel{
		Keys:    bson.M{"PublicID": 1},
		Options: options.Index().SetName("UQUsersPublicID").SetUnique(true),
	}
	_, _ = u.Indexes().CreateOne(context.Background(), UQUsersPublicID)

	UQUsersEmail := mongo.IndexModel{
		Keys:    bson.M{"Email": 1},
		Options: options.Index().SetName("UQUsersEmail").SetUnique(true),
	}
	_, _ = u.Indexes().CreateOne(context.Background(), UQUsersEmail)

	// ------------------------------------------------------------

	uaf := db.Collection("UsersAuthFailed")
	r.UsersAuthFailed = uaf

	TTLUsersAuthFailed := mongo.IndexModel{
		Keys:    bson.M{"ExpireDate": 1},
		Options: options.Index().SetName("TTL-UsersAuthFailed-ExpireDate").SetExpireAfterSeconds(0),
	}
	_, _ = uaf.Indexes().CreateOne(context.Background(), TTLUsersAuthFailed)

	// ------------------------------------------------------------

	c := db.Collection("UsersConfirmationTokens")
	r.Confirmations = c

	UQConfirmationsPublicID := mongo.IndexModel{
		Keys:    bson.M{"PublicID": 1},
		Options: options.Index().SetName("UQ-UsersConfirmationTokens-PublicID").SetUnique(true),
	}
	_, _ = c.Indexes().CreateOne(context.Background(), UQConfirmationsPublicID)

	UQConfirmationsToken := mongo.IndexModel{
		Keys:    bson.M{"Token": 1},
		Options: options.Index().SetName("UQ-UsersConfirmationTokens-Token").SetUnique(true),
	}
	_, _ = c.Indexes().CreateOne(context.Background(), UQConfirmationsToken)

	TTLConfirmationsExpireDate := mongo.IndexModel{
		Keys:    bson.M{"ExpireDate": 1},
		Options: options.Index().SetName("TTL-UsersConfirmationTokens-ExpireDate").SetExpireAfterSeconds(0),
	}
	_, _ = c.Indexes().CreateOne(context.Background(), TTLConfirmationsExpireDate)

	// ------------------------------------------------------------
	pr := db.Collection("UsersPasswordRecoveryTokens")
	r.PasswordRecovery = pr

	UQPasswordRecoveryPublicID := mongo.IndexModel{
		Keys:    bson.M{"PublicID": 1},
		Options: options.Index().SetName("UQ-UsersPasswordRecoveryTokens-PublicID").SetUnique(true),
	}
	_, _ = pr.Indexes().CreateOne(context.Background(), UQPasswordRecoveryPublicID)
	// if err != nil {
	// 	return nil, err
	// }

	UQPasswordRecoveryToken := mongo.IndexModel{
		Keys:    bson.M{"Token": 1},
		Options: options.Index().SetName("UQ-UsersPasswordRecoveryTokens-Token").SetUnique(true),
	}
	_, _ = pr.Indexes().CreateOne(context.Background(), UQPasswordRecoveryToken)
	// if err != nil {
	// 	return nil, err
	// }

	TTLPasswordRecoveryExpireDate := mongo.IndexModel{
		Keys:    bson.M{"ExpireDate": 1},
		Options: options.Index().SetName("TTL-UsersPasswordRecoveryTokens-ExpireDate").SetExpireAfterSeconds(0),
	}
	_, _ = pr.Indexes().CreateOne(context.Background(), TTLPasswordRecoveryExpireDate)
	// if err != nil {
	// 	return nil, err
	// }

	return &r, nil
}

func (svc *Service) CreateManager(ctx *audit.AuditContext) *Manager {
	return &Manager{
		AuditContext: ctx,
		Service:      svc,
	}
}

// CreateUser is the method to create a new user on the platform
func (m Manager) CreateUser(fullName string, email string, password string, passwordConf string) (string, error) {

	fullName = strings.TrimSpace(fullName)
	email = strings.ToLower(strings.TrimSpace(email))
	password = strings.TrimSpace(password)

	if err := m.CreateUserValidate(fullName, email, password, passwordConf); err != nil {
		return "", err
	}

	usr := User{
		Email:        email,
		FullName:     fullName,
		Salt:         GenerateID(16),
		IsActive:     true,
		CreatedDate:  time.Now().UTC(),
		PasswordDate: time.Now().UTC(),
	}
	usr.Password = m.hashPassword(usr.Salt, password)

	isValid := false
	for !isValid {
		usr.PublicID = GenerateID(8)

		isValid = !m.ExistPublicID(usr.PublicID)
	}

	_, err := m.Service.Users.InsertOne(context.Background(), usr)
	if err != nil {
		return "", err
	}

	if m.AuditContext != nil {
		if _, err := m.AuditContext.Audit(AuditModuleName, "CreateUser", usr.PublicID, "", ""); err != nil {
			panic(err)
		}
	}

	return usr.PublicID, nil
}
func (m Manager) CreateUserValidate(fullName string, email string, password string, passwordConf string) error {

	var items v.ErrorItems

	// fullName -------------------------------------------------------------
	if len(fullName) == 0 {

		items = append(items, v.ErrorItem{
			Field: "fullName",
			Rule:  v.Required,
		})

	} else {

		if len(fullName) < m.Service.Config.FullNameMinSize {
			items = append(items, v.ErrorItem{
				Field: "fullName",
				Rule:  v.Min,
				RuleValues: []interface{}{
					m.Service.Config.FullNameMinSize,
				},
			})
		}
		if len(fullName) > m.Service.Config.FullNameMaxSize {
			items = append(items, v.ErrorItem{
				Field: "fullName",
				Rule:  v.Max,
				RuleValues: []interface{}{
					m.Service.Config.FullNameMaxSize,
				},
			})
		}
		var rxName = regexp.MustCompile(`(?is)^(\p{Latin}| )+$`)
		if !rxName.MatchString(fullName) {
			items = append(items, v.ErrorItem{
				Field: "fullName",
				Rule:  v.OnlyLetters,
				RuleValues: []interface{}{
					"password",
				},
			})
		}

	}

	// email ----------------------------------------------------------------
	if len(email) == 0 {

		items = append(items, v.ErrorItem{
			Field: "email",
			Rule:  v.Required,
		})

	} else {

		if len(email) < m.Service.Config.EmailMinSize {
			items = append(items, v.ErrorItem{
				Field: "email",
				Rule:  v.Min,
				RuleValues: []interface{}{
					m.Service.Config.EmailMinSize,
				},
			})
		}
		if len(email) > m.Service.Config.EmailMaxSize {
			items = append(items, v.ErrorItem{
				Field: "email",
				Rule:  v.Max,
				RuleValues: []interface{}{
					m.Service.Config.EmailMaxSize,
				},
			})
		}
		var rxEmail = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
		if !rxEmail.MatchString(email) {
			items = append(items, v.ErrorItem{
				Field: "email",
				Rule:  v.InvalidEmail,
			})
		}

		if m.ExistEmail(email) {
			items = append(items, v.ErrorItem{
				Field: "email",
				Rule:  v.Duplicated,
			})
		}

	}

	// password -------------------------------------------------------------
	if password != passwordConf {
		items = append(items, v.ErrorItem{
			Field: "passwordConf",
			Rule:  v.ConfirmedPassword,
		})
	}

	if err := m.CheckPasswordStrength(password); err != nil {
		items = append(items, err...)
	}

	if len(passwordConf) == 0 {

		items = append(items, v.ErrorItem{
			Field: "passwordConf",
			Rule:  v.Required,
		})

	}

	if len(items) == 0 {
		return nil
	}
	return items
}

func (m Manager) UpdateUserName(publicID string, fullName string) error {
	var items v.ErrorItems

	name := strings.TrimSpace(fullName)

	if len(name) == 0 {

		items = append(items, v.ErrorItem{
			Field: "fullName",
			Rule:  v.Required,
		})

	} else {

		if len(name) < m.Service.Config.FullNameMinSize {
			items = append(items, v.ErrorItem{
				Field: "fullName",
				Rule:  v.Min,
				RuleValues: []interface{}{
					m.Service.Config.FullNameMinSize,
				},
			})
		}
		if len(name) > m.Service.Config.FullNameMaxSize {
			items = append(items, v.ErrorItem{
				Field: "fullName",
				Rule:  v.Max,
				RuleValues: []interface{}{
					m.Service.Config.FullNameMaxSize,
				},
			})
		}
		var rxName = regexp.MustCompile(`(?is)^(\p{Latin}| )+$`)
		if !rxName.MatchString(name) {
			items = append(items, v.ErrorItem{
				Field: "fullName",
				Rule:  v.OnlyLetters,
				RuleValues: []interface{}{
					"password",
				},
			})
		}

	}

	if !m.ExistPublicID(publicID) {
		items = append(items, v.ErrorItem{
			Field: "PublicID",
			Rule:  v.NotFound,
		})
	}

	if len(items) == 0 {

		filter := bson.M{"PublicID": publicID}
		upd := bson.M{"$set": bson.M{"FullName": name}}

		_, err := m.Service.Users.UpdateOne(context.Background(), filter, upd)
		if err != nil {
			return err
		}

		if m.AuditContext != nil {
			if _, err := m.AuditContext.Audit(AuditModuleName, "UpdateUserName", publicID, fmt.Sprintf("FullName changed to: %s", name), ""); err != nil {
				panic(err)
			}
		}
	}

	return nil
}
func (m Manager) UpdateUserActive(publicID string, isActive bool) error {
	var items v.ErrorItems

	if !m.ExistPublicID(publicID) {
		items = append(items, v.ErrorItem{
			Field: "PublicID",
			Rule:  v.NotFound,
		})
	}

	if len(items) == 0 {

		filter := bson.M{"PublicID": publicID}
		upd := bson.M{"$set": bson.M{"IsActive": isActive}}

		_, err := m.Service.Users.UpdateOne(context.Background(), filter, upd)
		if err != nil {
			return err
		}

		if m.AuditContext != nil {
			if _, err := m.AuditContext.Audit(AuditModuleName, "UpdateUserActive", publicID, fmt.Sprintf("IsActive to: %v", isActive), ""); err != nil {
				panic(err)
			}
		}
	}

	return nil
}
func (m Manager) UpdateUserMustResetPassword(publicID string, b bool) error {
	var items v.ErrorItems

	if !m.ExistPublicID(publicID) {
		items = append(items, v.ErrorItem{
			Field: "PublicID",
			Rule:  v.NotFound,
		})
	}

	if len(items) == 0 {

		filter := bson.M{"PublicID": publicID}
		upd := bson.M{"$set": bson.M{"MustResetPassword": b}}

		_, err := m.Service.Users.UpdateOne(context.Background(), filter, upd)
		if err != nil {
			return err
		}

		if m.AuditContext != nil {
			if _, err := m.AuditContext.Audit(AuditModuleName, "UpdateUserMustResetPassword", publicID, fmt.Sprintf("IsActive to: %v", b), ""); err != nil {
				panic(err)
			}
		}
	}

	return nil
}

func (m Manager) UpdateUserEmail(publicID string, newEmail string) error {
	var items v.ErrorItems

	email := strings.ToLower(strings.TrimSpace(newEmail))

	// check if the new email is acceptable
	if err := m.ValidateEmail(email); err != nil {
		var x v.ErrorItems = err.(v.ErrorItems)
		items = append(items, x...)
	}

	// Check if the email is already in use for other user
	if usr, err := m.GetUserByEmail(email); err != UserNotFound {
		panic(err)
	} else {
		// check if the user has a different
		if usr != nil && usr.PublicID != publicID {
			items = append(items, v.ErrorItem{
				Field: "email",
				Rule:  v.Duplicated,
			})
		}
	}

	// Check if the PublicID exists
	if usr, err := m.GetUserByPublicID(publicID); err != UserNotFound {
		panic(err)
	} else {
		// If the email remais equals, do nothing
		if usr.Email == email {
			return nil
		}
	}

	if len(items) == 0 {

		f := bson.M{"PublicID": publicID}
		u := bson.M{"$set": bson.M{"Email": email, "IsConfirmed": false, "ConfirmedDate": nil}}

		_, err := m.Service.Users.UpdateOne(context.Background(), f, u)
		if err != nil {
			return err
		}

		if m.AuditContext != nil {
			if _, err := m.AuditContext.Audit(AuditModuleName, "UpdateUserEmail", publicID, fmt.Sprintf("Email changed to: %s", email), ""); err != nil {
				panic(err)
			}
		}

	}

	return nil
}

func (m Manager) GetUserByPublicID(publicID string) (*User, error) {

	r := m.Service.Users.FindOne(context.Background(), bson.M{"PublicID": publicID})
	if err := r.Err(); err != nil {
		if mongo.ErrNoDocuments == err {
			return nil, UserNotFound
		}
		return nil, err
	}

	var usr User
	err := r.Decode(&usr)
	if err != nil {
		return nil, err
	}

	return &usr, nil
}
func (m Manager) GetUserByEmail(email string) (*User, error) {

	r := m.Service.Users.FindOne(context.Background(), bson.M{"Email": email})
	if err := r.Err(); err != nil {
		if mongo.ErrNoDocuments == err {
			return nil, UserNotFound
		}

		return nil, err
	}

	var usr User
	err := r.Decode(&usr)
	if err != nil {
		return nil, err
	}

	return &usr, nil
}
func (m Manager) SearchUsers(request SearchRequest) SearchResult {

	r := SearchResult{
		SearchRequest: request,
		Items:         nil,
		Total:         0,
	}

	QueryItems := []bson.M{}
	Filter := bson.M{}

	// PublicID             *string `bson:"PublicID"`
	if request.PublicID != nil && len(*request.PublicID) > 0 {
		QueryItems = append(QueryItems, bson.M{"PublicID": request.PublicID})
	}

	// Email             *string `bson:"Email"`
	if request.Email != nil && len(*request.Email) > 0 {
		QueryItems = append(QueryItems, bson.M{"Email": bson.M{"$regex": request.Email}})
	}

	// FullName          *string `bson:"FullName"`
	if request.FullName != nil && len(*request.FullName) > 0 {
		QueryItems = append(QueryItems, bson.M{"FullName": bson.M{"$regex": request.FullName}})
	}

	// IsActive          *bool   `bson:"IsActive"`
	if request.IsActive != nil {
		QueryItems = append(QueryItems, bson.M{"IsActive": bson.M{"$eq": *request.IsActive}})
	}

	// IsConfirmed       *bool   `bson:"IsConfirmed"`
	if request.IsConfirmed != nil {
		QueryItems = append(QueryItems, bson.M{"IsConfirmed": bson.M{"$eq": *request.IsConfirmed}})
	}

	// IsLocked          *bool   `bson:"IsLocked"`
	if request.IsLocked != nil {
		QueryItems = append(QueryItems, bson.M{"IsLocked": bson.M{"$eq": *request.IsLocked}})
	}

	// MustResetPassword *bool   `bson:"MustResetPassword"`
	if request.MustResetPassword != nil {
		QueryItems = append(QueryItems, bson.M{"MustResetPassword": bson.M{"$eq": *request.MustResetPassword}})
	}

	if len(QueryItems) > 0 {
		if len(QueryItems) == 1 {
			Filter = QueryItems[0]
		} else {
			Filter = bson.M{"$and": QueryItems}
		}
	}

	col := options.Collation{
		Locale:   "pt",
		Strength: 2,
	}

	opt := options.Count()
	opt.SetCollation(&col)

	if nCount, err := m.Service.Users.CountDocuments(context.Background(), Filter); err != nil {
		panic(err)
	} else {
		r.Total = nCount
	}

	if r.Total > 0 {

		ob := "FullName"
		dir := 1

		if request.OrderBy != nil && len(*request.OrderBy) > 0 {
			ob = *request.OrderBy
		}
		if request.OrderByDirection != nil && len(*request.OrderByDirection) > 0 {
			if *request.OrderByDirection == "desc" {
				dir = -1
			}
		}

		skip := (request.Page - 1) * request.PageSize

		opt := options.Find()
		opt.SetSort(bson.M{ob: dir})
		opt.SetSkip(int64(skip))
		opt.SetLimit(int64(request.PageSize))
		opt.SetCollation(&col)

		// var lst []User
		if cur, err := m.Service.Users.Find(context.Background(), Filter, opt); err != nil {
			panic(err)
		} else {
			if err := cur.All(context.TODO(), &r.Items); err != nil {
				panic(err)
			}
		}

	}

	// OrderBy           *string `bson:"OrderBy"`
	// OrderByDirection  *string `bson:"OrderByDirection"`

	return r
}

func (m Manager) Authenticate(email string, password string, onFail func(reason error), onSuccess func(user *User)) (bool, error) {

	usr, err := m.GetUserByEmail(email)
	if err != nil {
		if m.AuditContext != nil {
			if _, err := m.AuditContext.Audit(AuditModuleName, "Authenticate.Falied", "", "Email not found", email); err != nil {
				panic(err)
			}
		}
		if onFail != nil {
			onFail(err)
		}
		return false, err
	}

	if !usr.IsActive {
		if m.AuditContext != nil {
			if _, err := m.AuditContext.Audit(AuditModuleName, "Authenticate.Falied", usr.PublicID, "User is Inactive", email); err != nil {
				panic(err)
			}
		}
		if onFail != nil {
			onFail(UserInactive)
		}
		return false, UserInactive
	}
	if usr.IsLocked {
		if m.AuditContext != nil {
			if _, err := m.AuditContext.Audit(AuditModuleName, "Authenticate.Falied", usr.PublicID, "User is Locked", email); err != nil {
				panic(err)
			}
		}
		if onFail != nil {
			onFail(UserLocked)
		}
		return false, UserLocked
	}
	if m.IsUserSoftLocked(usr.PublicID) {
		if m.AuditContext != nil {
			if _, err := m.AuditContext.Audit(AuditModuleName, "Authenticate.UserSoftLocked", usr.PublicID, "User is temporarily locked", email); err != nil {
				panic(err)
			}
		}
		if onFail != nil {
			onFail(UserSoftLocked)
		}

		return false, UserSoftLocked
	}

	if usr.Password != m.hashPassword(usr.Salt, password) {

		if m.AuditContext != nil {
			if _, err := m.AuditContext.Audit(AuditModuleName, "Authenticate.Falied", usr.PublicID, "Wrong password", email); err != nil {
				panic(err)
			}
		}

		// Register Count of Failed Authentication Attempt
		if m.Service.Config.SoftLockAttempts > 0 {

			Count := usr.FailedAuthAttempts + 1
			f := bson.M{"PublicID": usr.PublicID}
			upd := bson.M{"$inc": bson.M{"FailedAuthAttempts": 1}}

			if Count >= m.Service.Config.SoftLockAttempts {

				// Create a Softlock record
				fail := AuthFail{
					PublicID:   usr.PublicID,
					CreateDate: time.Now().UTC(),
					ExpireDate: time.Now().Add(time.Second * time.Duration(m.Service.Config.SoftLockDurationSeconds)).UTC(),
				}

				if _, err := m.Service.UsersAuthFailed.InsertOne(context.Background(), fail); err != nil {
					panic(err)
				}

				// Sets the Attempts count to Zero
				upd = bson.M{"$set": bson.M{"FailedAuthAttempts": 0}}

				if m.AuditContext != nil {
					if _, err := m.AuditContext.Audit(AuditModuleName, "User.SoftLocked", usr.PublicID, "User authentication attempts was reached", fail); err != nil {
						panic(err)
					}
				}

			}

			result := m.Service.Users.FindOneAndUpdate(context.Background(), f, upd)
			if err := result.Err(); err != nil {
				panic(err)
			}

		}

		if onFail != nil {
			onFail(UserWrongPassword)
		}

		return false, UserWrongPassword
	}

	if m.AuditContext != nil {
		if _, err := m.AuditContext.Audit(AuditModuleName, "Authenticate.Success", usr.PublicID, "", email); err != nil {
			panic(err)
		}
	}

	if onSuccess != nil {
		onSuccess(usr)
	}

	return true, nil
}

func (m Manager) GenerateConfirmationToken(publicID string, replaceExisting bool) (string, error) {

	usr, err := m.GetUserByPublicID(publicID)
	if err != nil {
		return "", err
	}
	if usr == nil {
		return "", UserNotFound
	}
	if !usr.IsActive {
		return "", UserInactive
	}
	if usr.IsLocked {
		return "", UserLocked
	}

	token, err := m.GetConfirmationTokenByPublicID(publicID)
	if err != nil {
		return "", err
	}

	if token != nil && !replaceExisting {
		return "", ConfirmationTokenDuplicated
	}

	if token != nil {
		_, err := m.DeleteConfirmationToken(token.Token)
		if err != nil {
			return "", err
		}
	}

	NewConf := UserToken{}

	isValid := false
	for !isValid {
		NewConf.Token = GenerateID(32)

		exist, err := m.ExistsConfirmationToken(NewConf.Token)
		if err != nil {
			return "", err
		}

		isValid = !exist
	}

	NewConf.PublicID = usr.PublicID
	NewConf.CreateDate = time.Now().UTC()
	NewConf.ExpireDate = time.Now().Add(time.Duration(m.Service.Config.ConfirmationTokenTTLSeconds) * time.Second).UTC()

	_, err = m.Service.Confirmations.InsertOne(context.Background(), NewConf)
	if err != nil {
		return "", err
	}

	// UPDATE USER CONFIRMATION STATUS -----------------------------------------
	f := bson.M{"PublicID": usr.PublicID}
	upd := bson.M{"$set": bson.M{"IsConfirmed": false, "ConfirmedDate": nil}}

	result := m.Service.Users.FindOneAndUpdate(context.Background(), f, upd)
	if err := result.Err(); err != nil {
		return "", err
	}

	if m.AuditContext != nil {
		if _, err := m.AuditContext.Audit(AuditModuleName, "GenerateConfirmationToken", NewConf.Token, fmt.Sprintf("Expires at: %s", NewConf.ExpireDate), ""); err != nil {
			panic(err)
		}
	}
	return NewConf.Token, nil
}
func (m Manager) ExistsConfirmationToken(token string) (bool, error) {

	count, err := m.Service.Confirmations.CountDocuments(context.Background(), bson.M{"Token": token})
	if err != nil {
		return false, err
	}

	return count > 0, nil
}
func (m Manager) GetConfirmationToken(token string) (*UserToken, error) {

	r := m.Service.Confirmations.FindOne(context.Background(), bson.M{"Token": token})
	if err := r.Err(); err != nil {
		if mongo.ErrNoDocuments == err {
			return nil, nil
		}
		return nil, err
	}

	var tkn UserToken
	err := r.Decode(&tkn)
	if err != nil {
		return nil, err
	}

	return &tkn, nil
}
func (m Manager) GetConfirmationTokenByPublicID(publicID string) (*UserToken, error) {

	r := m.Service.Confirmations.FindOne(context.Background(), bson.M{"PublicID": publicID})
	if err := r.Err(); err != nil {
		if mongo.ErrNoDocuments == err {
			return nil, nil
		}
		return nil, err
	}

	var tkn UserToken
	err := r.Decode(&tkn)
	if err != nil {
		return nil, err
	}

	return &tkn, nil
}
func (m Manager) DeleteConfirmationToken(token string) (int64, error) {

	r, err := m.Service.Confirmations.DeleteOne(context.Background(), bson.M{"Token": token})
	if err != nil {
		return 0, err
	}

	if m.AuditContext != nil {
		if _, err := m.AuditContext.Audit(AuditModuleName, "DeleteConfirmationToken", token, "", ""); err != nil {
			panic(err)
		}
	}
	return r.DeletedCount, nil
}
func (m Manager) ConfirmToken(token string) (bool, error) {

	dbToken, err := m.GetConfirmationToken(token)
	if err != nil {
		return false, err
	}
	if dbToken == nil {
		return false, ConfirmationTokenNotFound
	}
	_, err = m.DeleteConfirmationToken(dbToken.Token)
	if err != nil {
		return false, err
	}

	usr, err := m.GetUserByPublicID(dbToken.PublicID)
	if err != nil {
		return false, err
	}
	if usr == nil {
		return false, UserNotFound
	}
	if !usr.IsActive {
		return false, UserInactive
	}
	if usr.IsLocked {
		return false, UserLocked
	}

	filter := bson.M{"PublicID": dbToken.PublicID}
	upd := bson.M{"$set": bson.M{"IsConfirmed": true, "ConfirmedDate": time.Now().UTC()}}
	dbRes, err := m.Service.Users.UpdateOne(context.Background(), filter, upd)
	if err != nil {
		return false, err
	}

	if m.AuditContext != nil {
		if _, err := m.AuditContext.Audit(AuditModuleName, "ConfirmToken", token, "", ""); err != nil {
			panic(err)
		}
	}
	return dbRes.ModifiedCount > 0, nil
}

func (m Manager) IsUserSoftLocked(publicID string) bool {

	if m.Service.Config.SoftLockAttempts > 0 {

		querySoftLock := bson.M{"PublicID": publicID}

		if count, err := m.Service.UsersAuthFailed.CountDocuments(context.Background(), querySoftLock); err != nil {
			panic(err)
		} else {
			return count > 0
		}

	}

	return false
}

func (m Manager) LockUser(publicID string) error {

	filter := bson.M{"PublicID": publicID}
	upd := bson.M{"$set": bson.M{"IsLocked": true, "LockedDate": time.Now().UTC()}}

	_, err := m.Service.Users.UpdateOne(context.Background(), filter, upd)
	if err != nil {
		return err
	}

	if m.AuditContext != nil {
		if _, err := m.AuditContext.Audit(AuditModuleName, "LockUser", publicID, "", ""); err != nil {
			panic(err)
		}
	}
	return nil
}
func (m Manager) UnLockUser(publicID string) error {

	filter := bson.M{"PublicID": publicID}
	upd := bson.M{"$set": bson.M{"IsLocked": false, "LockedDate": nil}}

	_, err := m.Service.Users.UpdateOne(context.Background(), filter, upd)
	if err != nil {
		return err
	}

	if m.AuditContext != nil {
		if _, err := m.AuditContext.Audit(AuditModuleName, "UnLockUser", publicID, "", ""); err != nil {
			panic(err)
		}
	}
	return nil
}

func (m Manager) ResetPassword(publicID string, oldPassword string, newPassword string, newPasswordConf string, checkOldPassword bool) error {

	var items v.ErrorItems

	usr, err := m.GetUserByPublicID(publicID)
	if err != nil && err != UserNotFound {
		panic(err)
	}

	if usr == nil {
		items = append(items, v.ErrorItem{
			Field:      "publicID",
			Rule:       v.NotFound,
			RuleValues: []interface{}{"publicID"},
		})
	} else {

		if checkOldPassword {
			if len(oldPassword) < 1 {
				items = append(items, v.ErrorItem{
					Field: "oldPassword",
					Rule:  v.Required,
				})
			} else {

				if m.hashPassword(usr.Salt, oldPassword) != usr.Password {
					items = append(items, v.ErrorItem{
						Field: "oldPassword",
						Rule:  v.WrongPassword,
					})
				}

			}
		}

		if err := m.CheckPasswordStrength(newPassword); err != nil {
			for _, e := range err {
				items = append(items, v.ErrorItem{
					Field:      "newPassword",
					Rule:       e.Rule,
					RuleValues: e.RuleValues,
				})
			}
		}

		if len(newPasswordConf) < 1 {
			items = append(items, v.ErrorItem{
				Field: "newPasswordConf",
				Rule:  v.Required,
			})
		} else {
			if newPassword != newPasswordConf {
				items = append(items, v.ErrorItem{
					Field: "newPasswordConf",
					Rule:  v.ConfirmedPassword,
				})
			}
		}

		if len(items) == 0 {

			salt := GenerateID(16)
			newPass := m.hashPassword(salt, newPassword)

			filter := bson.M{"PublicID": publicID}
			upd := bson.M{"$set": bson.M{"Salt": salt, "Password": newPass, "PasswordDate": time.Now().UTC(), "MustResetPassword": false}}

			_, err = m.Service.Users.UpdateOne(context.Background(), filter, upd)
			if err != nil {
				panic(err)
			}

			if m.AuditContext != nil {
				if _, err := m.AuditContext.Audit(AuditModuleName, "ResetPassword", publicID, "", ""); err != nil {
					panic(err)
				}
			}
		}

	}

	if len(items) == 0 {
		return nil
	}
	return items
}
func (m Manager) ResetPasswordForced(publicID string, newPassword string) error {

	if err := m.CheckPasswordStrength(newPassword); err != nil {
		return err
	}

	salt := GenerateID(16)
	newPass := m.hashPassword(salt, newPassword)

	filter := bson.M{"PublicID": publicID}
	upd := bson.M{"$set": bson.M{"Salt": salt, "Password": newPass, "PasswordDate": time.Now().UTC()}}

	_, err := m.Service.Users.UpdateOne(context.Background(), filter, upd)
	if err != nil {
		return err
	}

	if m.AuditContext != nil {
		if _, err := m.AuditContext.Audit(AuditModuleName, "ResetPasswordForced", publicID, "", ""); err != nil {
			panic(err)
		}
	}
	return nil
}

func (m Manager) ExistsPasswordRecoveryToken(token string) (bool, error) {

	count, err := m.Service.PasswordRecovery.CountDocuments(context.Background(), bson.M{"Token": token})
	if err != nil {
		return false, err
	}

	return count > 0, nil
}
func (m Manager) CreatePasswordRecoveryToken(publicID string) (string, error) {

	usr, err := m.GetUserByPublicID(publicID)
	if err != nil {
		return "", err
	}
	if usr == nil {
		return "", UserNotFound
	}
	if !usr.IsActive {
		return "", UserInactive
	}
	if usr.IsLocked {
		return "", UserLocked
	}

	// Remove Previous Tokens
	_, err = m.Service.PasswordRecovery.DeleteMany(context.Background(), bson.M{"PublicID": publicID})
	if err != nil {
		return "", err
	}

	// create new token
	NewConf := UserToken{}

	isValid := false
	for !isValid {
		NewConf.Token = GenerateID(32)

		exist, err := m.ExistsPasswordRecoveryToken(NewConf.Token)
		if err != nil {
			return "", err
		}

		isValid = !exist
	}

	NewConf.PublicID = usr.PublicID
	NewConf.CreateDate = time.Now().UTC()
	NewConf.ExpireDate = time.Now().Add(time.Duration(m.Service.Config.ConfirmationTokenTTLSeconds) * time.Second).UTC()

	_, err = m.Service.PasswordRecovery.InsertOne(context.Background(), NewConf)
	if err != nil {
		return "", err
	}

	if m.AuditContext != nil {
		if _, err := m.AuditContext.Audit(AuditModuleName, "CreatePasswordRecoveryToken", NewConf.Token, fmt.Sprintf("PublicID: %s", NewConf.PublicID), ""); err != nil {
			panic(err)
		}
	}
	return NewConf.Token, nil
}
func (m Manager) GetPasswordRecoveryToken(token string, deleteFromDB bool) (*UserToken, error) {

	var r UserToken

	tk := m.Service.PasswordRecovery.FindOne(context.Background(), bson.M{"Token": token})
	if err := tk.Err(); err != nil {
		if mongo.ErrNoDocuments == err {
			return nil, ResetPasswordTokenNotFound
		}
		return nil, err
	}

	err := tk.Decode(&r)
	if err != nil {
		return nil, err
	}

	if deleteFromDB {
		delRes := m.Service.PasswordRecovery.FindOneAndDelete(context.Background(), bson.M{"Token": token})
		if err := delRes.Err(); err != nil {
			return nil, err
		}
	}

	return &r, nil
}
func (m Manager) GetPasswordRecoveryTokenByPublicID(publicID string, deleteFromDB bool) (*UserToken, error) {

	var r UserToken

	tk := m.Service.PasswordRecovery.FindOne(context.Background(), bson.M{"PublicID": publicID})
	if err := tk.Err(); err != nil {
		return nil, err
	}

	err := tk.Decode(&r)
	if err != nil {
		return nil, err
	}

	if deleteFromDB {
		m.Service.PasswordRecovery.FindOneAndDelete(context.Background(), bson.M{"PublicID": publicID})
	}

	return &r, nil
}
func (m Manager) DeletePasswordRecoveryToken(token string) (int64, error) {

	r, err := m.Service.PasswordRecovery.DeleteOne(context.Background(), bson.M{"Token": token})
	if err != nil {
		return 0, err
	}

	if m.AuditContext != nil {
		if _, err := m.AuditContext.Audit(AuditModuleName, "DeletePasswordRecoveryToken", token, "", ""); err != nil {
			panic(err)
		}
	}
	return r.DeletedCount, nil
}
func (m Manager) DeletePasswordRecoveryTokenByPublicID(publicID string) (int64, error) {

	r, err := m.Service.PasswordRecovery.DeleteOne(context.Background(), bson.M{"PublicID": publicID})
	if err != nil {
		return 0, err
	}

	if m.AuditContext != nil {
		if _, err := m.AuditContext.Audit(AuditModuleName, "DeletePasswordRecoveryTokenByPublicID", publicID, "", ""); err != nil {
			panic(err)
		}
	}
	return r.DeletedCount, nil
}

func (m Manager) CheckPasswordStrength(password string) v.ErrorItems {

	var items v.ErrorItems
	pInfo := GetStringInfo(password)

	if pInfo.Len == 0 {

		items = append(items, v.ErrorItem{
			Field: "password",
			Rule:  v.Required,
		})

	} else {

		if pInfo.Len < m.Service.Config.PasswordMinSize {
			items = append(items, v.ErrorItem{
				Field: "password",
				Rule:  v.Min,
				RuleValues: []interface{}{
					m.Service.Config.PasswordMinSize,
				},
			})
		}
		if pInfo.Len > m.Service.Config.PasswordMaxSize {
			items = append(items, v.ErrorItem{
				Field: "password",
				Rule:  v.Max,
				RuleValues: []interface{}{
					m.Service.Config.PasswordMaxSize,
				},
			})
		}
		if pInfo.LettersUppercase < m.Service.Config.PasswordMinUppercaseLetters {
			items = append(items, v.ErrorItem{
				Field: "password",
				Rule:  v.MinUppercase,
				RuleValues: []interface{}{
					m.Service.Config.PasswordMinUppercaseLetters,
				},
			})
		}
		if pInfo.LettersLowercase < m.Service.Config.PasswordMinLowercaseLetters {
			items = append(items, v.ErrorItem{
				Field: "password",
				Rule:  v.MinLowercase,
				RuleValues: []interface{}{
					m.Service.Config.PasswordMinLowercaseLetters,
				},
			})
		}
		if pInfo.Numbers < m.Service.Config.PasswordMinNumbers {
			items = append(items, v.ErrorItem{
				Field: "password",
				Rule:  v.MinNumeric,
				RuleValues: []interface{}{
					m.Service.Config.PasswordMinNumbers,
				},
			})
		}
		if pInfo.Symbols < m.Service.Config.PasswordMinSymbols {
			items = append(items, v.ErrorItem{
				Field: "password",
				Rule:  v.MinSymbols,
				RuleValues: []interface{}{
					m.Service.Config.PasswordMinSymbols,
				},
			})
		}

	}

	if len(items) == 0 {
		return nil
	}
	return items
}
func (m Manager) hashPassword(salt string, pass string) string {
	pw := pbkdf2.Key([]byte(pass), []byte(salt), 4096, 32, sha512.New)
	return base64.StdEncoding.EncodeToString(pw)
}

func (m Manager) ExistPublicID(publicID string) bool {

	filter := bson.M{"PublicID": publicID}

	opt := options.Find()
	opt.SetLimit(1)

	if c, err := m.Service.Users.Find(context.Background(), filter, opt); err != nil {
		panic(err)
	} else {
		return c.RemainingBatchLength() > 0
	}

}

func (m Manager) ExistEmail(email string) bool {

	filter := bson.M{"Email": email}

	opt := options.Find()
	opt.SetLimit(1)

	if c, err := m.Service.Users.Find(context.Background(), filter, opt); err != nil {
		panic(err)
	} else {
		return c.RemainingBatchLength() > 0
	}

}

func (m Manager) ValidateEmail(email string) error {
	var items v.ErrorItems

	if len(email) == 0 {

		items = append(items, v.ErrorItem{
			Field: "email",
			Rule:  v.Required,
		})

	} else {

		if len(email) < m.Service.Config.EmailMinSize {
			items = append(items, v.ErrorItem{
				Field: "email",
				Rule:  v.Min,
				RuleValues: []interface{}{
					m.Service.Config.EmailMinSize,
				},
			})
		}
		if len(email) > m.Service.Config.EmailMaxSize {
			items = append(items, v.ErrorItem{
				Field: "email",
				Rule:  v.Max,
				RuleValues: []interface{}{
					m.Service.Config.EmailMaxSize,
				},
			})
		}
		var rxEmail = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
		if !rxEmail.MatchString(email) {
			items = append(items, v.ErrorItem{
				Field: "email",
				Rule:  v.InvalidEmail,
			})
		}

	}

	if len(items) == 0 {
		return nil
	}
	return items
}
