package users

import (
	"testing"
)

func TestGenerateID(t *testing.T) {

	results := make([]string, 100)
	for i := 0; i < len(results); i++ {
		results[i] = GenerateID(10)
	}

	CountDifferent := make(map[string]int)
	for i := 0; i < len(results); i++ {
		CountDifferent[results[i]]++
	}

	if len(results) != len(CountDifferent) {
		t.Fatal("repetidos")
	}
}

func TestGetStringInfo(t *testing.T) {

	a := "123456"
	b := "abcdef"
	c := "ABCDEF"
	d := "|!#$%&/()=?»«`"
	e := "à"

	A := GetStringInfo(a)
	B := GetStringInfo(b)
	C := GetStringInfo(c)
	D := GetStringInfo(d)
	E := GetStringInfo(e)

	if A.Len != len(a) || A.Numbers != 6 {
		t.Fail()
	}
	if B.Len != len(b) || B.LettersLowercase != 6 {
		t.Fail()
	}
	if C.Len != len(c) || C.LettersUppercase != 6 {
		t.Fail()
	}
	if D.Len != len(d) || D.Symbols != 14 {
		t.Fail()
	}
	if E.Len != len(e) {
		t.Fail()
	}

}
