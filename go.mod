module bitbucket.org/flaviocfoliveira/users

go 1.14

require (
	bitbucket.org/flaviocfoliveira/audit v1.0.7
	github.com/aws/aws-sdk-go v1.37.1 // indirect
	github.com/golang/snappy v0.0.2 // indirect
	github.com/klauspost/compress v1.11.7 // indirect
	go.mongodb.org/mongo-driver v1.4.5
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a // indirect
	golang.org/x/text v0.3.5 // indirect
)
