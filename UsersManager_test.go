package users

import (
	"context"
	"testing"
	"time"

	"bitbucket.org/flaviocfoliveira/audit"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	v "bitbucket.org/flaviocfoliveira/users/UsersValidation"
)

const (
	// conn string = "mongodb://usrMship:000000@10.2.2.21:27017/Membership"
	conn      string = "mongodb://usrMship:000000@192.168.1.89:27017/Membership"
	connAudit string = "mongodb://auditUser:auditPass@192.168.1.89:27017/auditor"
)

func TestUsersManager_CreateUser_SUCCESS(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)

	// ACT ----------------------------------------
	id, err := scope.Manager.CreateUser("Manager CreateUser SUCCESS", "geral123@flaviooliveira.com", "!123Ab4560!", "!123Ab4560!")
	if err != nil {
		scope.Cleanup()
		t.Fatalf("Could not create Users: %s", err)
	}
	scope.PublicIDList = append(scope.PublicIDList, id)

	// ASSERT -------------------------------------
	if len(id) == 0 {
		scope.Cleanup()
		t.Fatal("the PublicID was not returned")
	}

	scope.Cleanup()
}
func TestUsersManager_CreateUser_fullName_validation(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)

	// ACT ----------------------------------------
	_, errReq := scope.Manager.CreateUser("", "", "", "")
	_, errMin := scope.Manager.CreateUser("few", "", "", "")
	_, errMax := scope.Manager.CreateUser("1234567890123456789012345678901234567890", "", "", "")
	_, errInvalid := scope.Manager.CreateUser("123456789012345", "", "", "")

	// ASSERT -------------------------------------
	if !errReq.(v.ErrorItems).Contains("fullName", v.Required) {
		scope.Cleanup()
		t.Fatal("fullName-Required")
	}
	if !errMin.(v.ErrorItems).Contains("fullName", v.Min) {
		scope.Cleanup()
		t.Fatal("fullName-Min")
	}
	if !errMax.(v.ErrorItems).Contains("fullName", v.Max) {
		scope.Cleanup()
		t.Fatal("fullName-Max")
	}
	if !errInvalid.(v.ErrorItems).Contains("fullName", v.OnlyLetters) {
		scope.Cleanup()
		t.Fatal("fullName-OnlyLetters")
	}

	scope.Cleanup()
}
func TestUsersManager_CreateUser_email_validation(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	usr := scope.GenerateUser(false)
	usr.Email = "geral123@flaviooliveira.com"
	scope.AddUserToDatabase(usr)

	// ACT ----------------------------------------
	_, errReq := scope.Manager.CreateUser("", "", "", "")
	_, errMin := scope.Manager.CreateUser("", "few", "", "")
	_, errMax := scope.Manager.CreateUser("", "1234567890123456789012345678901234567890", "", "")
	_, errInvalid := scope.Manager.CreateUser("", "12345678901234567890", "", "")
	_, errDup := scope.Manager.CreateUser("", usr.Email, "", "")

	// ASSERT -------------------------------------
	if !errReq.(v.ErrorItems).Contains("email", v.Required) {
		scope.Cleanup()
		t.Fatal("email-Required")
	}
	if !errMin.(v.ErrorItems).Contains("email", v.Min) {
		scope.Cleanup()
		t.Fatal("email-Min")
	}
	if !errMax.(v.ErrorItems).Contains("email", v.Max) {
		scope.Cleanup()
		t.Fatal("email-Max")
	}
	if !errInvalid.(v.ErrorItems).Contains("email", v.InvalidEmail) {
		scope.Cleanup()
		t.Fatal("email-InvalidEmail")
	}
	if !errDup.(v.ErrorItems).Contains("email", v.Duplicated) {
		scope.Cleanup()
		t.Fatal("email-Duplicated")
	}

	scope.Cleanup()
}
func TestUsersManager_CreateUser_password_validation(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)

	// ACT ----------------------------------------
	_, errReq := scope.Manager.CreateUser("", "", "", "")
	_, errLenMin := scope.Manager.CreateUser("", "", "00", "00")
	_, errLenMax := scope.Manager.CreateUser("", "", "1234567890123456789012345678901234567890", "1234567890123456789012345678901234567890")
	_, errMinUpper := scope.Manager.CreateUser("", "", "aaaaaaaaaa", "aaaaaaaaaa")
	_, errMinLower := scope.Manager.CreateUser("", "", "AAAAAAAAA", "AAAAAAAAA")
	_, errMinNum := scope.Manager.CreateUser("", "", "AbCdEfGhIj", "AbCdEfGhIj")
	_, errMinSymb := scope.Manager.CreateUser("", "", "123ABCdef", "123ABCdef")
	_, errEqual := scope.Manager.CreateUser("", "", "ABCdef123", "123ABCdef")

	// ASSERT -------------------------------------
	if !errReq.(v.ErrorItems).Contains("password", v.Required) {
		scope.Cleanup()
		t.Fatal("password-Required")
	}
	if !errReq.(v.ErrorItems).Contains("passwordConf", v.Required) {
		scope.Cleanup()
		t.Fatal("passwordConf-Required")
	}
	if !errLenMin.(v.ErrorItems).Contains("password", v.Min) {
		scope.Cleanup()
		t.Fatal("password-Min")
	}
	if !errLenMax.(v.ErrorItems).Contains("password", v.Max) {
		scope.Cleanup()
		t.Fatal("password-Max")
	}
	if !errMinUpper.(v.ErrorItems).Contains("password", v.MinUppercase) {
		scope.Cleanup()
		t.Fatal("password-MinUppercase")
	}
	if !errMinLower.(v.ErrorItems).Contains("password", v.MinLowercase) {
		scope.Cleanup()
		t.Fatal("password-MinLowercase")
	}
	if !errMinNum.(v.ErrorItems).Contains("password", v.MinNumeric) {
		scope.Cleanup()
		t.Fatal("password-MinNumeric")
	}
	if !errMinSymb.(v.ErrorItems).Contains("password", v.MinSymbols) {
		scope.Cleanup()
		t.Fatal("password-MinSymbols")
	}
	if !errEqual.(v.ErrorItems).Contains("passwordConf", v.ConfirmedPassword) {
		scope.Cleanup()
		t.Fatal("passwordConf-ConfirmedPassword")
	}

}

func TestUsersManager_Authenticate_SUCCESS(t *testing.T) {

	// ARRANGE ------------------------------------
	pw := "!123Ab4560!"
	scope := setup(t)
	id, err := scope.Manager.CreateUser("Authenticate SUCCESS", "geral123@flaviooliveira.com", pw, pw)
	if err != nil {
		t.Fatalf("Could not validate Users: %s", err)
	}
	scope.PublicIDList = append(scope.PublicIDList, id)

	// ACT ----------------------------------------
	validated, err := scope.Manager.Authenticate("geral123@flaviooliveira.com", pw, nil, nil)
	if err != nil {
		scope.Cleanup()
		t.Fatalf("Could not Authenticate User: %s", err)
	}

	// ASSERT -------------------------------------
	if !validated {
		scope.Cleanup()
		t.Fail()
	}

	scope.Cleanup()
}
func TestUsersManager_Authenticate_USERNOTFOUND(t *testing.T) {

	// ARRANGE ------------------------------------
	pw := "!123Ab4560!"
	scope := setup(t)

	// ACT ----------------------------------------
	validated, err := scope.Manager.Authenticate("not.existing@email.com", pw, nil, nil)

	// ASSERT -------------------------------------
	if err != UserNotFound {
		t.Fatal(UserNotFound)
	}

	if validated {
		t.Fatal("Authenticate-USERNOTFOUND")
	}

}
func TestUsersManager_Authenticate_USERINACTIVE(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	expected := scope.GenerateUser(false)
	expected.IsActive = false
	scope.AddUserToDatabase(expected)

	// ACT ----------------------------------------
	validated, err := scope.Manager.Authenticate(expected.Email, "pw", nil, nil)

	// ASSERT -------------------------------------
	if err != UserInactive {
		scope.Cleanup()
		t.Fatal(UserInactive)
	}

	if validated {
		scope.Cleanup()
		t.Fatal("Authenticate-USERINACTIVE")
	}

	scope.Cleanup()
}
func TestUsersManager_Authenticate_USERLOCKED(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	expected := scope.GenerateUser(false)
	expected.IsLocked = true
	scope.AddUserToDatabase(expected)

	// ACT ----------------------------------------
	validated, err := scope.Manager.Authenticate(expected.Email, "pw", nil, nil)

	// ASSERT -------------------------------------
	if err != UserLocked {
		scope.Cleanup()
		t.Fatal(UserLocked)
	}

	if validated {
		scope.Cleanup()
		t.Fatal("Authenticate-USERLOCKED")
	}

	scope.Cleanup()
}
func TestUsersManager_Authenticate_WONRGPASSWORD(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	expected := scope.GenerateUser(false)
	expected.Email = "geral123@flaviooliveira.com"
	scope.AddUserToDatabase(expected)

	// ACT ----------------------------------------
	validated, err := scope.Manager.Authenticate("geral123@flaviooliveira.com", "WrOnGpAsSwOrD", nil, nil)

	// ASSERT -------------------------------------
	if err != UserWrongPassword {
		scope.Cleanup()
		t.Fatal(UserWrongPassword)
	}
	if validated {
		scope.Cleanup()
		t.Fatal("Authenticate-WONRGPASSWORD")
	}

	scope.Cleanup()
}
func TestUsersManager_Authenticate_SOFTLOCKUSER(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	expected := scope.GenerateUser(false)
	expected.Email = "geral123@flaviooliveira.com"
	scope.AddUserToDatabase(expected)

	// ACT ----------------------------------------
	_, _ = scope.Manager.Authenticate("geral123@flaviooliveira.com", "WrOnGpAsSwOrD", nil, nil)
	_, _ = scope.Manager.Authenticate("geral123@flaviooliveira.com", "WrOnGpAsSwOrD", nil, nil)
	_, _ = scope.Manager.Authenticate("geral123@flaviooliveira.com", "WrOnGpAsSwOrD", nil, nil)
	_, _ = scope.Manager.Authenticate("geral123@flaviooliveira.com", "WrOnGpAsSwOrD", nil, nil)

	// ASSERT -------------------------------------
	querySoftLock := bson.M{"PublicID": expected.PublicID}
	if count, err := scope.Service.UsersAuthFailed.CountDocuments(context.Background(), querySoftLock); err != nil {
		scope.Cleanup()
		t.Fatal(err)
	} else {
		if count < 1 {
			scope.Cleanup()
			t.Fatal("Authenticate-SOFTLOCKUSER")
		}
	}

	scope.Cleanup()
}

func TestUsersManager_GetUserByPublicID_SUCCESS(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	expected := scope.GenerateUser(true)

	// ACT ----------------------------------------
	actual, err := scope.Manager.GetUserByPublicID(expected.PublicID)

	// ASSERT -------------------------------------
	if actual == nil || err != nil {
		t.Fail()
	}

	scope.Cleanup()
}
func TestUsersManager_GetUserByPublicID_NOTFOUND(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)

	// ACT ----------------------------------------
	actual, err := scope.Manager.GetUserByPublicID("NO.existing.PublicID")

	// ASSERT -------------------------------------
	if actual != nil || err != UserNotFound {
		t.Fatal(UserNotFound)
	}

}

func TestUsersManager_SearchUsers_SUCCESS(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	expected := scope.GenerateUser(true)

	filter := SearchRequest{
		PublicID: &expected.PublicID,
		Email:    &expected.Email,
	}

	// ACT ----------------------------------------
	actual := scope.Manager.SearchUsers(filter)

	// ASSERT -------------------------------------
	if actual.Total == 0 {
		t.Fatal(UserNotFound)
	}

}
func TestUsersManager_SearchUsers_ALL_SUCCESS(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)

	filter := SearchRequest{
		PublicID:          nil,
		Email:             nil,
		FullName:          nil,
		IsActive:          nil,
		IsConfirmed:       nil,
		IsLocked:          nil,
		MustResetPassword: nil,
		OrderBy:           nil,
		OrderByDirection:  nil,
		Page:              1,
		PageSize:          3,
	}

	// ACT ----------------------------------------
	actual := scope.Manager.SearchUsers(filter)

	// ASSERT -------------------------------------
	if actual.Total == 0 {
		t.Fatal(UserNotFound)
	}

}

func TestUsersManager_GetUserByEmail_SUCCESS(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	expected := scope.GenerateUser(true)

	// ACT ----------------------------------------
	actual, err := scope.Manager.GetUserByEmail(expected.Email)

	// ASSERT -------------------------------------
	if actual == nil || err != nil {
		t.Fail()
	}

	scope.Cleanup()
}
func TestUsersManager_GetUserByEmail_NOTFOUND(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)

	// ACT ----------------------------------------
	actual, err := scope.Manager.GetUserByEmail("NO.existing.PublicID")

	// ASSERT -------------------------------------
	if actual != nil || err != UserNotFound {
		t.Fatal(UserNotFound)
	}

	scope.Cleanup()
}

func TestUsersManager_ExistsConfirmationToken_SUCCESS(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	expected := scope.GenerateRandomConfirmationToken(true, "")

	// ACT ----------------------------------------
	exists, err := scope.Manager.ExistsConfirmationToken(expected.Token)

	// ASSERT -------------------------------------
	if !exists || err != nil {
		scope.Cleanup()
		t.Fail()
	}

	scope.Cleanup()
}
func TestUsersManager_ExistsConfirmationToken_NOTFOUND(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)

	// ACT ----------------------------------------
	exists, err := scope.Manager.ExistsConfirmationToken("not.exists")

	// ASSERT -------------------------------------
	if exists || err != nil {
		t.Fail()
	}

}

func TestUsersManager_GetConfirmationToken_SUCCESS(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	expected := scope.GenerateRandomConfirmationToken(true, "")

	// ACT ----------------------------------------
	actual, err := scope.Manager.GetConfirmationToken(expected.Token)

	// ASSERT -------------------------------------
	if actual == nil || err != nil {
		scope.Cleanup()
		t.Fail()
	}

	if actual != nil && expected.Token != actual.Token {
		t.Fail()
	}
	if actual != nil && expected.PublicID != actual.PublicID {
		t.Fail()
	}

	scope.Cleanup()
}
func TestUsersManager_GetConfirmationToken_NOTFOUND(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)

	// ACT ----------------------------------------
	actual, err := scope.Manager.GetConfirmationToken("expected.Token")

	// ASSERT -------------------------------------
	if actual != nil || err != nil {
		t.Fail()
	}

}

func TestUsersManager_GetConfirmationTokenByPublicID_SUCCESS(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	expected := scope.GenerateRandomConfirmationToken(true, "")

	// ACT ----------------------------------------
	actual, err := scope.Manager.GetConfirmationTokenByPublicID(expected.PublicID)

	// ASSERT -------------------------------------
	if actual == nil || err != nil {
		scope.Cleanup()
		t.Fail()
	}

	if actual != nil && expected.Token != actual.Token {
		t.Fail()
	}
	if actual != nil && expected.PublicID != actual.PublicID {
		t.Fail()
	}

	scope.Cleanup()
}
func TestUsersManager_GetConfirmationTokenByPublicID_NOTFOUND(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)

	// ACT ----------------------------------------
	actual, err := scope.Manager.GetConfirmationTokenByPublicID("expected.PublicID")

	// ASSERT -------------------------------------
	if actual != nil || err != nil {
		t.Fail()
	}

}

func TestUsersManager_DeleteConfirmationToken_SUCCESS(t *testing.T) {
	// ARRANGE ------------------------------------
	scope := setup(t)
	expected := scope.GenerateRandomConfirmationToken(true, "")

	// ACT ----------------------------------------
	count, err := scope.Manager.DeleteConfirmationToken(expected.Token)

	// ASSERT -------------------------------------
	if count != 1 || err != nil {
		scope.Cleanup()
		t.Fail()
	}

	dbCount, err := scope.Manager.Service.Confirmations.CountDocuments(context.TODO(), bson.M{"Token": expected.Token})
	if dbCount > 0 || err != nil {
		scope.Cleanup()
		t.Fail()
	}

	scope.Cleanup()
}

func TestUsersManager_GenerateConfirmationToken_SUCCESS(t *testing.T) {
	// ARRANGE ------------------------------------
	scope := setup(t)
	expected := scope.GenerateUser(true)

	// ACT ----------------------------------------
	token, err := scope.Manager.GenerateConfirmationToken(expected.PublicID, true)
	if err != nil {
		t.Fail()
	}
	scope.ConfirmTokenList = append(scope.ConfirmTokenList, token)

	// ASSERT -------------------------------------
	if len(token) == 0 {
		scope.Cleanup()
		t.Fail()
	}

	dbCount, err := scope.Manager.Service.Confirmations.CountDocuments(context.TODO(), bson.M{"Token": token})
	if dbCount != 1 || err != nil {
		scope.Cleanup()
		t.Fail()
	}

	dbRes := scope.Manager.Service.Users.FindOne(context.TODO(), bson.M{"PublicID": expected.PublicID})
	if err := dbRes.Err(); err != nil {
		scope.Cleanup()
		t.Fail()
	}

	var u User
	err = dbRes.Decode(&u)
	if err != nil {
		scope.Cleanup()
		t.Fail()
	}

	if u.IsConfirmed || u.ConfirmedDate != nil {
		scope.Cleanup()
		t.Fail()
	}

	scope.Cleanup()
}
func TestUsersManager_GenerateConfirmationToken_USERNOTFOUND(t *testing.T) {
	// ARRANGE ------------------------------------
	scope := setup(t)

	// ACT ----------------------------------------
	token, err := scope.Manager.GenerateConfirmationToken("not.found.user.id", true)
	if err != UserNotFound {
		scope.Cleanup()
		t.Fatalf("Unexpected error, %s", err)
	}

	if len(token) > 0 {
		scope.Cleanup()
		t.Fatal("token should not be returned since PublicID should not exist")
	}

	scope.Cleanup()
}
func TestUsersManager_GenerateConfirmationToken_USERINACTIVE(t *testing.T) {
	// ARRANGE ------------------------------------
	scope := setup(t)
	expected := scope.GenerateUser(false)
	expected.IsActive = false
	scope.AddUserToDatabase(expected)

	// ACT ----------------------------------------
	token, err := scope.Manager.GenerateConfirmationToken(expected.PublicID, true)
	if err != UserInactive {
		scope.Cleanup()
		t.Fatalf("Unexpected error, %s", err)
	}

	if len(token) > 0 {
		scope.Cleanup()
		t.Fatal("token should not be returned since PublicID should not exist")
	}

	scope.Cleanup()
}
func TestUsersManager_GenerateConfirmationToken_USERLOCKED(t *testing.T) {
	// ARRANGE ------------------------------------
	scope := setup(t)
	expected := scope.GenerateUser(false)
	expected.IsLocked = true
	scope.AddUserToDatabase(expected)

	// ACT ----------------------------------------
	token, err := scope.Manager.GenerateConfirmationToken(expected.PublicID, true)
	if err != UserLocked {
		scope.Cleanup()
		t.Fatalf("Unexpected error, %s", err)
	}

	if len(token) > 0 {
		scope.Cleanup()
		t.Fatal("token should not be returned since PublicID should not exist")
	}

	scope.Cleanup()
}
func TestUsersManager_GenerateConfirmationToken_DUPLCATEDTOKEN(t *testing.T) {
	// ARRANGE ------------------------------------

	scope := setup(t)
	expected := scope.GenerateUser(true)
	scope.GenerateRandomConfirmationToken(true, expected.PublicID)

	// ACT ----------------------------------------
	token, err := scope.Manager.GenerateConfirmationToken(expected.PublicID, false)
	if err != ConfirmationTokenDuplicated {
		scope.Cleanup()
		t.Fatalf("Unexpected error, %s", err)
	}

	if len(token) > 0 {
		scope.Cleanup()
		t.Fatal("token should not be returned since PublicID should not exist")
	}

	scope.Cleanup()
}

func TestUsersManager_ConfirmToken_SUCCESS(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	usr := scope.GenerateUser(false)
	usr.IsActive = true
	usr.IsLocked = false
	usr.IsConfirmed = false
	usr.ConfirmedDate = nil
	scope.AddUserToDatabase(usr)

	tk := scope.GenerateRandomConfirmationToken(true, usr.PublicID)

	// ACT ----------------------------------------
	confirmed, err := scope.Manager.ConfirmToken(tk.Token)

	// ASSERT -------------------------------------
	if err != nil {
		scope.Cleanup()
		t.Fatal(err)
	}

	if !confirmed {
		t.Fatal("Token was not confirmed")
	}

	u := scope.getDBUser(usr.PublicID)
	if u == nil {
		t.Fatal("User not found")
	} else {
		if !u.IsConfirmed {
			t.Fatal("User was not properly confirmed")
		}
		if u.ConfirmedDate == nil {
			t.Fatal("User ConfirmedDate was not properly set")
		}
	}

	scope.Cleanup()
}
func TestUsersManager_ConfirmToken_TOKENNOTFOUND(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)

	// ACT ----------------------------------------
	confirmed, err := scope.Manager.ConfirmToken("tk.Token")

	// ASSERT -------------------------------------
	if err != ConfirmationTokenNotFound {
		scope.Cleanup()
		t.Fatal(err)
	}

	if confirmed {
		t.Fatal("Token should not be confirmed")
	}

	scope.Cleanup()
}
func TestUsersManager_ConfirmToken_USERNOTFOUND(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	tk := scope.GenerateRandomConfirmationToken(true, "")

	// ACT ----------------------------------------
	confirmed, err := scope.Manager.ConfirmToken(tk.Token)

	// ASSERT -------------------------------------
	if err != UserNotFound {
		scope.Cleanup()
		t.Fatal(err)
	}

	if confirmed {
		t.Fatal("Token should not be confirmed")
	}

	scope.Cleanup()
}
func TestUsersManager_ConfirmToken_USERINACTIVE(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	expected := scope.GenerateUser(false)
	expected.IsActive = false
	scope.AddUserToDatabase(expected)

	tk := scope.GenerateRandomConfirmationToken(true, expected.PublicID)

	// ACT ----------------------------------------
	confirmed, err := scope.Manager.ConfirmToken(tk.Token)

	// ASSERT -------------------------------------
	if err != UserInactive {
		scope.Cleanup()
		t.Fatal(err)
	}

	if confirmed {
		t.Fatal("Token should not be confirmed")
	}

	scope.Cleanup()
}
func TestUsersManager_ConfirmToken_USERLOCKED(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	expected := scope.GenerateUser(false)
	expected.IsLocked = true
	scope.AddUserToDatabase(expected)

	tk := scope.GenerateRandomConfirmationToken(true, expected.PublicID)

	// ACT ----------------------------------------
	confirmed, err := scope.Manager.ConfirmToken(tk.Token)

	// ASSERT -------------------------------------
	if err != UserLocked {
		scope.Cleanup()
		t.Fatal(err)
	}

	if confirmed {
		t.Fatal("Token should not be confirmed")
	}

	scope.Cleanup()
}

func TestUsersManager_LockUser_SUCCESS(t *testing.T) {
	// ARRANGE ------------------------------------
	scope := setup(t)
	usr := scope.GenerateUser(false)
	usr.IsLocked = false
	usr.LockedDate = nil
	scope.AddUserToDatabase(usr)

	// ACT ----------------------------------------
	err := scope.Manager.LockUser(usr.PublicID)

	// ASSERT -------------------------------------
	if err != nil {
		t.Fatal(err)
	}

	dbUsr := scope.getDBUser(usr.PublicID)
	if !dbUsr.IsLocked {
		scope.Cleanup()
		t.Fatal("failed to lock user")
	}
	if dbUsr.LockedDate == nil {
		scope.Cleanup()
		t.Fatal("failed to register lock date")
	}

	scope.Cleanup()
}
func TestUsersManager_UnLockUser_SUCCESS(t *testing.T) {
	// ARRANGE ------------------------------------
	scope := setup(t)
	usr := scope.GenerateUser(false)
	usr.IsLocked = true
	dt := time.Now().UTC()
	usr.LockedDate = &dt
	scope.AddUserToDatabase(usr)

	// ACT ----------------------------------------
	err := scope.Manager.UnLockUser(usr.PublicID)

	// ASSERT -------------------------------------
	if err != nil {
		t.Fatal(err)
	}

	dbUsr := scope.getDBUser(usr.PublicID)
	if dbUsr.IsLocked {
		scope.Cleanup()
		t.Fatal("failed to unlock user")
	}
	if dbUsr.LockedDate != nil {
		scope.Cleanup()
		t.Fatal("failed to clear lock date")
	}

	scope.Cleanup()
}

func TestUsersManager_ResetPasswordForced_SUCCESS(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	usr := scope.GenerateUser(true)

	// ACT ----------------------------------------
	err := scope.Manager.ResetPasswordForced(usr.PublicID, "#123abcDEF#")

	// ASSERT -------------------------------------
	if err != nil {
		scope.Cleanup()
		t.Fatal(err)
	}

	usrDb := scope.getDBUser(usr.PublicID)

	if usr.Password == usrDb.Password {
		scope.Cleanup()
		t.Fatal("password was not changed")
	}
	if usr.Salt == usrDb.Salt {
		scope.Cleanup()
		t.Fatal("password was not changed")
	}

	scope.Cleanup()

}
func TestUsersManager_ResetPasswordForced_WEAKPASSWORD(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)

	// ACT ----------------------------------------
	errLenMin := scope.Manager.ResetPasswordForced("FakeID", "00")
	errLenMax := scope.Manager.ResetPasswordForced("FakeID", "1234567890123456789012345678901234567890")
	errMinUpper := scope.Manager.ResetPasswordForced("FakeID", "aaaaaaaaaa")
	errMinLower := scope.Manager.ResetPasswordForced("FakeID", "AAAAAAAAA")
	errMinNum := scope.Manager.ResetPasswordForced("FakeID", "AbCdEfGhIj")
	errMinSymb := scope.Manager.ResetPasswordForced("FakeID", "123ABCdef")

	// ASSERT -------------------------------------
	if errLenMin == nil {
		t.Fatal(errLenMin)
	}
	if errLenMax == nil {
		t.Fatal(errLenMax)
	}
	if errMinUpper == nil {
		t.Fatal(errMinUpper)
	}
	if errMinLower == nil {
		t.Fatal(errMinLower)
	}
	if errMinNum == nil {
		t.Fatal(errMinNum)
	}
	if errMinSymb == nil {
		t.Fatal(errMinSymb)
	}

}
func TestUsersManager_ExistsPasswordRecoveryToken_SUCCESS(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	prt := scope.GenerateRandomPasswordRecoveryToken(true, "FLAVIO")

	// ACT ----------------------------------------
	existsTrue, err := scope.Manager.ExistsPasswordRecoveryToken(prt.Token)
	if err != nil {
		scope.Cleanup()
		t.Fatal(err)
	}

	existsFalse, err := scope.Manager.ExistsPasswordRecoveryToken("prt.Token")
	if err != nil {
		scope.Cleanup()
		t.Fatal(err)
	}

	// ASSERT -------------------------------------
	if existsTrue != true {
		scope.Cleanup()
		t.Fatal("Could not verify if token exists")
	}
	if existsFalse != false {
		scope.Cleanup()
		t.Fatal("Could not verify if token not exists")
	}

	scope.Cleanup()
}
func TestUsersManager_CreatePasswordRecoveryToken_SUCCESS(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	usr := scope.GenerateUser(false)
	usr.IsActive = true
	usr.IsLocked = false
	scope.AddUserToDatabase(usr)

	// ACT ----------------------------------------
	token, err := scope.Manager.CreatePasswordRecoveryToken(usr.PublicID)

	// ASSERT -------------------------------------
	if err != nil {
		scope.Cleanup()
		t.Fatal(err)
	}

	if len(token) == 0 {
		scope.Cleanup()
		t.Fatal("didnt return a token")
	}

	tk := scope.getPasswordRecoveryToken(token)
	if tk == nil {
		scope.Cleanup()
		t.Fatal("token was not generated on the database")
	}

	if tk.PublicID != usr.PublicID {
		scope.Cleanup()
		t.Fatal("token does not belong to the expected user")
	}

	scope.Cleanup()
}

func TestUsersManager_ResetPassword_SUCCESS(t *testing.T) {
	// ARRANGE ------------------------------------
	scope := setup(t)
	pwd := "!123ABCdef#"
	PublicID, err := scope.Manager.CreateUser("Flávio CF Oliveira", "geral123@flaviooliveira.com", pwd, pwd)
	if err != nil {
		scope.Cleanup()
		t.Fatal(err)
	}
	scope.PublicIDList = append(scope.PublicIDList, PublicID)
	usr := scope.getDBUser(PublicID)

	// ACT ----------------------------------------
	newPwd := "$abc123DEF$"
	err = scope.Manager.ResetPassword(PublicID, pwd, newPwd, newPwd, true)

	// ASSERT -------------------------------------
	if err != nil {
		scope.Cleanup()
		t.Fatal(err)
	}

	usrDb := scope.getDBUser(PublicID)

	if usr.Password == usrDb.Password {
		scope.Cleanup()
		t.Fatal("password was not changed")
	}
	if usr.Salt == usrDb.Salt {
		scope.Cleanup()
		t.Fatal("password was not changed")
	}

	scope.Cleanup()
}
func TestUsersManager_ResetPassword_USERNOTFOUND(t *testing.T) {
	// ARRANGE ------------------------------------
	scope := setup(t)

	// ACT ----------------------------------------
	err := scope.Manager.ResetPassword("NOT.EXISTING.ID", "???NOPassw0rd", "$abc123DEF$", "$abc123DEF$", true)

	// ASSERT -------------------------------------
	if !err.(v.ErrorItems).Contains("publicID", v.NotFound) {
		t.Fatal("publicID-NotFound")
	}

}
func TestUsersManager_ResetPassword_WRONGOLDPASSWORD(t *testing.T) {
	// ARRANGE ------------------------------------
	scope := setup(t)
	pwd := "!123ABCdef#"
	PublicID, err := scope.Manager.CreateUser("Flávio CF Oliveira", "geral123@flaviooliveira.com", pwd, pwd)
	if err != nil {
		scope.Cleanup()
		t.Fatal(err)
	}
	scope.PublicIDList = append(scope.PublicIDList, PublicID)

	// ACT ----------------------------------------
	err = scope.Manager.ResetPassword(PublicID, "WRONG.PASSWORD", "$abc123DEF$", "$abc123DEF$", true)

	// ASSERT -------------------------------------
	if !err.(v.ErrorItems).Contains("oldPassword", v.WrongPassword) {
		scope.Cleanup()
		t.Fatal(err)
	}

	scope.Cleanup()
}
func TestUsersManager_ResetPassword_NEWPASSWORDWEAK(t *testing.T) {
	// ARRANGE ------------------------------------
	scope := setup(t)
	pwd := "!123ABCdef#"
	PublicID, err := scope.Manager.CreateUser("Flávio CF Oliveira", "geral123@flaviooliveira.com", pwd, pwd)
	if err != nil {
		scope.Cleanup()
		t.Fatal(err)
	}
	scope.PublicIDList = append(scope.PublicIDList, PublicID)

	// ACT ----------------------------------------
	errReq := scope.Manager.ResetPassword(PublicID, "", "", "", true)
	errLenMin := scope.Manager.ResetPassword(PublicID, "00", "00", "00", true)
	errLenMax := scope.Manager.ResetPassword(PublicID, "1234567890123456789012345678901234567890", "1234567890123456789012345678901234567890", "1234567890123456789012345678901234567890", true)
	errMinUpper := scope.Manager.ResetPassword(PublicID, "aaaaaaaaaa", "aaaaaaaaaa", "aaaaaaaaaa", true)
	errMinLower := scope.Manager.ResetPassword(PublicID, "AAAAAAAAA", "AAAAAAAAA", "AAAAAAAAA", true)
	errMinNum := scope.Manager.ResetPassword(PublicID, "AbCdEfGhIj", "AbCdEfGhIj", "AbCdEfGhIj", true)
	errMinSymb := scope.Manager.ResetPassword(PublicID, "123ABCdef", "123ABCdef", "123ABCdef", true)

	// ASSERT -------------------------------------
	if !errReq.(v.ErrorItems).Contains("newPassword", v.Required) {
		scope.Cleanup()
		t.Fatal("newPassword-Required")
	}
	if !errLenMin.(v.ErrorItems).Contains("newPassword", v.Min) {
		scope.Cleanup()
		t.Fatal("newPassword-Min")
	}
	if !errLenMax.(v.ErrorItems).Contains("newPassword", v.Max) {
		scope.Cleanup()
		t.Fatal("newPassword-Max")
	}
	if !errMinUpper.(v.ErrorItems).Contains("newPassword", v.MinUppercase) {
		scope.Cleanup()
		t.Fatal("newPassword-MinUppercase")
	}
	if !errMinLower.(v.ErrorItems).Contains("newPassword", v.MinLowercase) {
		scope.Cleanup()
		t.Fatal("newPassword-MinLowercase")
	}
	if !errMinNum.(v.ErrorItems).Contains("newPassword", v.MinNumeric) {
		scope.Cleanup()
		t.Fatal("newPassword-MinNumeric")
	}
	if !errMinSymb.(v.ErrorItems).Contains("newPassword", v.MinSymbols) {
		scope.Cleanup()
		t.Fatal("newPassword-MinSymbols")
	}

	scope.Cleanup()
}
func TestUsersManager_ResetPassword_NEWPASSWORDNOTMATCH(t *testing.T) {
	// ARRANGE ------------------------------------
	scope := setup(t)
	pwd := "!123ABCdef#"
	PublicID, err := scope.Manager.CreateUser("Flávio CF Oliveira", "geral123@flaviooliveira.com", pwd, pwd)
	if err != nil {
		scope.Cleanup()
		t.Fatal(err)
	}
	scope.PublicIDList = append(scope.PublicIDList, PublicID)

	// ACT ----------------------------------------
	errReq := scope.Manager.ResetPassword(PublicID, pwd, "#123$456Aa#", "#123$456$AbC#", true)

	// ASSERT -------------------------------------
	if !errReq.(v.ErrorItems).Contains("newPasswordConf", v.ConfirmedPassword) {
		scope.Cleanup()
		t.Fatal("newPasswordConf-ConfirmedPassword")
	}

	scope.Cleanup()
}

func TestUsersManager_GetPasswordRecoveryToken_SUCCESS(t *testing.T) {
	// ARRANGE ------------------------------------
	scope := setup(t)
	prt1 := scope.GenerateRandomPasswordRecoveryToken(true, "aaa")
	prt2 := scope.GenerateRandomPasswordRecoveryToken(true, "bbb")

	// ACT ----------------------------------------
	dbRes1, err1 := scope.Manager.GetPasswordRecoveryToken(prt1.Token, false)
	dbRes2, err2 := scope.Manager.GetPasswordRecoveryToken(prt2.Token, true)
	dbRes3, err3 := scope.Manager.GetPasswordRecoveryToken("NOTFOUNDTOKEN", false)

	// ASSERT -------------------------------------
	if err1 != nil {
		scope.Cleanup()
		t.Fatal(err1)
	}
	if err2 != nil {
		scope.Cleanup()
		t.Fatal(err2)
	}

	if dbRes1.Token != prt1.Token {
		scope.Cleanup()
		t.Fatal("got wrong token 1")
	}
	if dbRes2.Token != prt2.Token {
		scope.Cleanup()
		t.Fatal("got wrong token 2")
	}

	if dbRes1.PublicID != prt1.PublicID {
		scope.Cleanup()
		t.Fatal("got wrong PublicID 1")
	}
	if dbRes2.PublicID != prt2.PublicID {
		scope.Cleanup()
		t.Fatal("got wrong PublicID 2")
	}

	if dbRes3 != nil || err3 != ResetPasswordTokenNotFound {
		scope.Cleanup()
		t.Fatal("Wrong response for a notfoundToke,")
	}

	tk2 := scope.getPasswordRecoveryToken(prt2.Token)
	if tk2 != nil {
		scope.Cleanup()
		t.Fatal("token was not deleted during get token")
	}

	scope.Cleanup()
}
func TestUsersManager_GetPasswordRecoveryTokenByPublicID_SUCCESS(t *testing.T) {
	// ARRANGE ------------------------------------
	scope := setup(t)
	prt1 := scope.GenerateRandomPasswordRecoveryToken(true, "aaa")
	prt2 := scope.GenerateRandomPasswordRecoveryToken(true, "bbb")

	// ACT ----------------------------------------
	dbRes1, err1 := scope.Manager.GetPasswordRecoveryTokenByPublicID(prt1.PublicID, false)
	dbRes2, err2 := scope.Manager.GetPasswordRecoveryTokenByPublicID(prt2.PublicID, true)

	// ASSERT -------------------------------------
	if err1 != nil {
		scope.Cleanup()
		t.Fatal(err1)
	}
	if err2 != nil {
		scope.Cleanup()
		t.Fatal(err2)
	}

	if dbRes1.Token != prt1.Token {
		scope.Cleanup()
		t.Fatal("got wrong token 1")
	}
	if dbRes2.Token != prt2.Token {
		scope.Cleanup()
		t.Fatal("got wrong token 2")
	}

	if dbRes1.PublicID != prt1.PublicID {
		scope.Cleanup()
		t.Fatal("got wrong PublicID 1")
	}
	if dbRes2.PublicID != prt2.PublicID {
		scope.Cleanup()
		t.Fatal("got wrong PublicID 2")
	}

	tk2 := scope.getPasswordRecoveryToken(prt2.Token)
	if tk2 != nil {
		scope.Cleanup()
		t.Fatal("token was not deleted during get token")
	}

	scope.Cleanup()
}

func TestUsersManager_DeletePasswordRecoveryToken_SUCCESS(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	prt1 := scope.GenerateRandomPasswordRecoveryToken(true, "aaa")

	// ACT ----------------------------------------
	count1, err1 := scope.Manager.DeletePasswordRecoveryToken(prt1.Token)
	count2, err2 := scope.Manager.DeletePasswordRecoveryToken("unexistentToken")

	// ASSERT -------------------------------------
	if err1 != nil {
		scope.Cleanup()
		t.Fatal(err1)
	}
	if err2 != nil {
		scope.Cleanup()
		t.Fatal(err2)
	}

	if count1 < 1 {
		scope.Cleanup()
		t.Fatal("error returning detele affected 1")
	}
	if count2 != 0 {
		scope.Cleanup()
		t.Fatal("error returning detele affected 2")
	}

	tk2 := scope.getPasswordRecoveryToken(prt1.Token)
	if tk2 != nil {
		scope.Cleanup()
		t.Fatal("token was not deleted ")
	}

	scope.Cleanup()
}
func TestUsersManager_DeletePasswordRecoveryTokenByPublicID_SUCCESS(t *testing.T) {
	// ARRANGE ------------------------------------
	scope := setup(t)
	prt1 := scope.GenerateRandomPasswordRecoveryToken(true, "aaa")

	// ACT ----------------------------------------
	count1, err1 := scope.Manager.DeletePasswordRecoveryTokenByPublicID(prt1.PublicID)
	count2, err2 := scope.Manager.DeletePasswordRecoveryTokenByPublicID("unexistentPublicID")

	// ASSERT -------------------------------------
	if err1 != nil {
		scope.Cleanup()
		t.Fatal(err1)
	}
	if err2 != nil {
		scope.Cleanup()
		t.Fatal(err2)
	}

	if count1 < 1 {
		scope.Cleanup()
		t.Fatal("error returning detele affected 1")
	}
	if count2 != 0 {
		scope.Cleanup()
		t.Fatal("error returning detele affected 2")
	}

	tk2 := scope.getPasswordRecoveryToken(prt1.Token)
	if tk2 != nil {
		scope.Cleanup()
		t.Fatal("token was not deleted ")
	}

	scope.Cleanup()
}

func TestUsersManager_ExistPublicID_SUCCESS(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	usr := scope.GenerateUser(true)

	// ACT ----------------------------------------
	exist := scope.Manager.ExistPublicID(usr.PublicID)
	notExist := scope.Manager.ExistPublicID("ABCenasID")

	// ASSERT -------------------------------------
	if !exist {
		scope.Cleanup()
		t.Fatal(exist)
	}
	if notExist {
		scope.Cleanup()
		t.Fatal(notExist)
	}

	scope.Cleanup()
}

func TestUsersManager_ExistEmail_SUCCESS(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	usr := scope.GenerateUser(true)

	// ACT ----------------------------------------
	exist := scope.Manager.ExistEmail(usr.Email)
	notExist := scope.Manager.ExistEmail("ABCenasID")

	// ASSERT -------------------------------------
	if !exist {
		scope.Cleanup()
		t.Fatal(exist)
	}
	if notExist {
		scope.Cleanup()
		t.Fatal(notExist)
	}

	scope.Cleanup()
}

func TestUsersManager_UpdateUserMustResetPassword_SUCCESS(t *testing.T) {

	// ARRANGE ------------------------------------
	scope := setup(t)
	usr := scope.GenerateUser(true)

	// ACT ----------------------------------------
	must := scope.Manager.UpdateUserMustResetPassword(usr.PublicID, true)
	u := scope.getDBUser(usr.PublicID)

	// ASSERT -------------------------------------
	if must != nil {
		scope.Cleanup()
		t.Fatal(must)
	}
	if u.MustResetPassword == false {
		scope.Cleanup()
		t.Fatal("Found wrong on db")
	}

	// ACT2 ----------------------------------------
	must2 := scope.Manager.UpdateUserMustResetPassword(usr.PublicID, false)
	u2 := scope.getDBUser(usr.PublicID)

	// ASSERT2 -------------------------------------
	if must2 != nil {
		scope.Cleanup()
		t.Fatal(must)
	}
	if u2.MustResetPassword == true {
		scope.Cleanup()
		t.Fatal("Found wrong on db")
	}

	scope.Cleanup()
}

/*


AUX


*/
type testScope struct {
	T                    *testing.T
	Config               *Settings
	Service              *Service
	Manager              *Manager
	PublicIDList         []string
	ConfirmTokenList     []string
	PasswordRecoveryList []string
	AuditService         *audit.AuditService
	AuditContext         *audit.AuditContext
}

func (this *testScope) GenerateUser(insertOnDB bool) User {
	r := User{}

	r.PublicID = GenerateID(8)
	r.Email = GenerateID(8)
	r.FullName = GenerateID(8)
	r.Password = GenerateID(8)
	r.Salt = GenerateID(8)
	r.IsActive = true
	r.CreatedDate = time.Now().UTC()
	r.IsConfirmed = true
	confDate := time.Now().UTC()
	r.ConfirmedDate = &confDate
	r.IsLocked = false
	r.LockedDate = nil
	r.MustResetPassword = false

	if insertOnDB {
		this.AddUserToDatabase(r)
	}

	return r
}
func (this *testScope) AddUserToDatabase(u User) {

	_, err := this.Manager.Service.Users.InsertOne(context.TODO(), u)
	if err != nil {
		panic(err)
	}

	this.PublicIDList = append(this.PublicIDList, u.PublicID)
}
func (this *testScope) getDBUser(publicID string) *User {

	dbRes := this.Manager.Service.Users.FindOne(context.TODO(), bson.M{"PublicID": publicID})
	if err := dbRes.Err(); err != nil {
		panic(err)
	}

	var u User
	err := dbRes.Decode(&u)
	if err != nil {
		panic(err)
	}

	return &u
}

func (this *testScope) GenerateRandomConfirmationToken(insertOnDB bool, publicID string) UserToken {
	r := UserToken{}

	if len(publicID) > 0 {
		r.PublicID = publicID
	} else {
		r.PublicID = GenerateID(16)
	}
	r.Token = GenerateID(32)
	r.CreateDate = time.Now().UTC()
	r.ExpireDate = time.Now().Add(time.Duration(this.Config.ConfirmationTokenTTLSeconds) * time.Second).UTC()

	if insertOnDB {
		this.AddConfirmationTokenToDatabase(r)
	}

	return r
}
func (this *testScope) AddConfirmationTokenToDatabase(token UserToken) {

	_, err := this.Manager.Service.Confirmations.InsertOne(context.TODO(), token)
	if err != nil {
		panic(err)
	}

	this.ConfirmTokenList = append(this.ConfirmTokenList, token.Token)
}

func (this *testScope) GenerateRandomPasswordRecoveryToken(insertOnDB bool, publicID string) UserToken {
	r := UserToken{}

	if len(publicID) > 0 {
		r.PublicID = publicID
	} else {
		r.PublicID = GenerateID(16)
	}
	r.Token = GenerateID(32)
	r.CreateDate = time.Now().UTC()
	r.ExpireDate = time.Now().Add(time.Duration(this.Config.ConfirmationTokenTTLSeconds) * time.Second).UTC()

	if insertOnDB {
		this.AddPasswordRecoveryTokenToDatabase(r)
	}

	return r
}
func (this *testScope) AddPasswordRecoveryTokenToDatabase(token UserToken) {

	_, err := this.Manager.Service.PasswordRecovery.InsertOne(context.TODO(), token)
	if err != nil {
		panic(err)
	}

	this.PasswordRecoveryList = append(this.PasswordRecoveryList, token.Token)
}
func (this *testScope) getPasswordRecoveryToken(token string) *UserToken {

	dbRes := this.Manager.Service.PasswordRecovery.FindOne(context.TODO(), bson.M{"Token": token})
	if err := dbRes.Err(); err != nil {
		if mongo.ErrNoDocuments == err {
			return nil
		}
		panic(err)
	}

	var t UserToken
	err := dbRes.Decode(&t)
	if err != nil {
		panic(err)
	}

	return &t
}

var as *audit.AuditService

func setup(t *testing.T) *testScope {
	r := testScope{}
	r.T = t

	r.Config = &Settings{
		ConfirmationTokenTTLSeconds: 60,
		FullNameMinSize:             6,
		FullNameMaxSize:             32,
		EmailMinSize:                10,
		EmailMaxSize:                32,
		PasswordMinSize:             6,
		PasswordMaxSize:             32,
		PasswordMinUppercaseLetters: 1,
		PasswordMinLowercaseLetters: 1,
		PasswordMinNumbers:          1,
		PasswordMinSymbols:          1,
		SoftLockAttempts:            3,
		SoftLockDurationSeconds:     60,
	}

	m, err := NewUsersService(conn, r.Config)
	if err != nil {
		t.Fatalf("Error during test setup: %s", err)
	}

	aSettings := audit.AuditSettings{
		ConnectionString: connAudit,
		ServiceName:      "Users.Testing",
	}

	if as == nil {
		if svc, err := audit.NewAuditService(aSettings); err != nil {
			panic(err)
		} else {
			as = svc
		}
	}
	r.AuditService = as

	r.Service = m
	r.Manager = m.CreateManager(r.AuditService.CreatAuditContext("10.10.1.1", "Machine-Name-XXXX", "UserX"))

	return &r
}
func (this testScope) Cleanup() {

	if len(this.PublicIDList) > 0 {
		for _, publicID := range this.PublicIDList {
			_, err := this.Manager.Service.Users.DeleteOne(context.Background(), bson.M{"PublicID": publicID})
			if err != nil {
				this.T.Fatal(err)
			}
		}
	}

	if len(this.ConfirmTokenList) > 0 {
		for _, token := range this.ConfirmTokenList {
			_, err := this.Manager.Service.Confirmations.DeleteOne(context.Background(), bson.M{"Token": token})
			if err != nil {
				this.T.Fatal(err)
			}
		}
	}

	if len(this.PasswordRecoveryList) > 0 {
		for _, token := range this.PasswordRecoveryList {
			_, err := this.Manager.Service.PasswordRecovery.DeleteOne(context.Background(), bson.M{"Token": token})
			if err != nil {
				this.T.Fatal(err)
			}
		}
	}

}

// // ARRANGE ------------------------------------
// scope := setup(t)
//
// // ACT ----------------------------------------
//
//
// // ASSERT -------------------------------------
//
// scope.Cleanup()
