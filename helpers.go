package users

import (
	"math/rand"
	"time"
	"unicode"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"

var seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

func GenerateID(size int) string {
	b := make([]byte, size)
	for i := range b {
		b[i] = letterBytes[seededRand.Intn(len(letterBytes))]
	}
	return string(b)
}

type StringInfo struct {
	Len              int
	Numbers          int
	LettersUppercase int
	LettersLowercase int
	Symbols          int
}

func GetStringInfo(s string) StringInfo {
	r := StringInfo{}
	r.Len = len(s)

	if r.Len > 0 {
		for _, char := range s {
			switch {
			case unicode.IsUpper(char):
				r.LettersUppercase++
			case unicode.IsLower(char):
				r.LettersLowercase++
			case unicode.IsDigit(char):
				r.Numbers++
			case unicode.IsPunct(char) || unicode.IsSymbol(char):
				r.Symbols++
			}
		}
	}

	return r
}
